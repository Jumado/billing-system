﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Billing_system
{
    /// <summary>
    /// Interaction logic for ServerSetup.xaml
    /// </summary>
    public partial class ServerSetup : Page
    {
        public ServerSetup()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(this.txtServer.Text.Trim())) {
                MessageBox.Show("Please provide a server name or IP address", "Server", MessageBoxButton.OK, MessageBoxImage.Warning);   
            } else if(string.IsNullOrEmpty(this.txtDatabase.Text.Trim() )){
                MessageBox.Show("Please provide the database name", "Database", MessageBoxButton.OK, MessageBoxImage.Warning);  
            }else if (string.IsNullOrEmpty (this.txtUserid.Text.Trim())){
                MessageBox.Show("Please provide the database server user id", "User id", MessageBoxButton.OK, MessageBoxImage.Warning);  
            }else if(string.IsNullOrEmpty(this.txtpassword.Password.Trim ())){
                MessageBox.Show("Please provide the database server login password", "Password", MessageBoxButton.OK, MessageBoxImage.Warning);  
            }else{
                try {
                    Properties.Settings.Default.server = this.txtServer.Text.Trim();
                    Properties.Settings.Default.database = this.txtDatabase.Text.Trim();
                    Properties.Settings.Default.userid = this.txtUserid.Text.Trim();
                    Properties.Settings.Default.password = this.txtpassword.Password.Trim();
                    Properties.Settings.Default.firstrun = false; 
                    Properties.Settings.Default.Save();
                    MessageBox.Show("Server settings saved successfully", "Server settings", MessageBoxButton.OK, MessageBoxImage.Information);
                    Properties.Settings.Default.Reload();  
                } catch {
                    MessageBox.Show("Server settings saved successfully", "Server settings", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            
            }
        }

        private void txtpassword_KeyUp(object sender, KeyEventArgs e)
        {
           
                if (!string.IsNullOrEmpty(this.txtpassword.Password.Trim()))
                {
                    if (e.Key == Key.Enter) {
                        btnSave_Click(sender, e);
                    }
                }
          
        }
    }
}
