﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;  
namespace Billing_system
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       public  Timer UpdateTimer = new Timer();
       public delegate void UpdateTime();
        public MainWindow()
        {
            InitializeComponent();
            UpdateTimer.Interval = 1000;
            UpdateTimer.Enabled = true;
            UpdateTimer.Elapsed += new ElapsedEventHandler(UpdateTimer_Elapsed);
            this.lstbxitmServersetup.MouseUp += new MouseButtonEventHandler(lstbxitmServersetup_MouseUp);
            this.lstbxitmOrderplacement.MouseUp += new MouseButtonEventHandler(lstbxitmOrderplacement_MouseUp);
            if (Properties.Settings.Default.firstrun) {
                ServerSetup setuppage = new ServerSetup();
                this.DisplayFrame.Navigate(setuppage);
            }
            
        }

        void lstbxitmOrderplacement_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try{
                Pizzaorderpage Pizzapage = new Pizzaorderpage();
                this.DisplayFrame.Navigate(Pizzapage);  
            }
            catch {
                MessageBox.Show("An error ocurred while loading pizza ordering page", "Pizza order", MessageBoxButton.OK, MessageBoxImage.Exclamation);    
            }
        }

        void lstbxitmServersetup_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try{

                ServerSetup setuppage = new ServerSetup();
                this.DisplayFrame.Navigate(setuppage);
            }
            catch {
                MessageBox.Show("Sever setup page failed to load due to an error", "Server setup", MessageBoxButton.OK, MessageBoxImage.Exclamation); 
            }
        }

        void UpdateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            Dispatcher.Invoke(new UpdateTime(Updateuitime));  
        }
        void Updateuitime() {
            try
            {
                this.txtblkcurrent.Text = DateTime.Now.ToString("dddd  MMMM d,yyyy" + "   " + "HH:mm:ss").ToUpper();  
            }
            catch { 
            
            }
        }
    }
}
