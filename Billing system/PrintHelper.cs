﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
namespace Billing_system
{
   public static class PrintHelper
    {
       public static FlowDocument CreateFlowDocument(PizzaOrder order) {
           FlowDocument doc = new FlowDocument();
           Section Headersec = new Section();
           Paragraph P1 = new Paragraph();
           Bold bld = new Bold();
           bld.Inlines.Add(new Run("PIZZA PALACE ORDER RECEIPT"));
           Underline underlinedbld = new Underline();
           underlinedbld.Inlines.Add(bld ); 
           P1.Inlines.Add(underlinedbld);
           Headersec.Blocks.Add(P1);
           Headersec.TextAlignment = System.Windows.TextAlignment.Center;
           Headersec.FontFamily = new System.Windows.Media.FontFamily("Cambria");
           doc.Blocks.Add(Headersec);
           Section Bodysec = new Section();
           Paragraph P2 = new Paragraph();
           P2.Margin = new System.Windows.Thickness(20, 1, 1, 1);
           foreach (PizzaOrderDetail ord in order.PizzaOrderDetails) {
              
               P2.Inlines.Add(new Run(string.Concat(ord.Pieces, " ", ord.Pizzadescription, ": $", ord.TotalPizzacost.ToString("0.00"), Environment.NewLine)));
           }
          
           Bodysec.Blocks.Add(P2);
           Bodysec.TextAlignment = System.Windows.TextAlignment.Justify;
           Bodysec.FontFamily= new System.Windows.Media.FontFamily("Cambria");
           doc.Blocks.Add(Bodysec);
           Section Footersec = new Section();
           Footersec.TextAlignment = System.Windows.TextAlignment.Justify;
           Footersec.FontFamily = new System.Windows.Media.FontFamily("Cambria");
           Paragraph P3 = new Paragraph();
           P3.Margin= new System.Windows.Thickness(20,1,1,1);
           P3.Inlines.Add(new Run(string.Concat("Subtotal $: ",order.Subtotal.ToString("0.00"),Environment.NewLine )));
           P3.Inlines.Add(new Run(string.Concat("GST $:", order.GST.ToString("0.00"), Environment.NewLine)));
           P3.Inlines.Add(new Run(string.Concat("Total $: ", order.Total.ToString("0.00"), Environment.NewLine)));
           Footersec.Blocks.Add(P3);
           doc.Blocks.Add(Footersec);
           return doc;
       }
    }
}
