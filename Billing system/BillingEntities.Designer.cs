﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
#region EDM Relationship Metadata

[assembly: EdmRelationshipAttribute("BillingModel", "FK_PizzaOrderDetails_PizzaOrders", "PizzaOrder", System.Data.Metadata.Edm.RelationshipMultiplicity.One, typeof(Billing_system.PizzaOrder), "PizzaOrderDetail", System.Data.Metadata.Edm.RelationshipMultiplicity.Many, typeof(Billing_system.PizzaOrderDetail), true)]

#endregion

namespace Billing_system
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class Entities : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new Entities object using the connection string found in the 'Entities' section of the application configuration file.
        /// </summary>
        public Entities() : base("name=Entities", "Entities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new Entities object.
        /// </summary>
        public Entities(string connectionString) : base(connectionString, "Entities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new Entities object.
        /// </summary>
        public Entities(EntityConnection connection) : base(connection, "Entities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
        #region ObjectSet Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<PizzaOrder> PizzaOrders
        {
            get
            {
                if ((_PizzaOrders == null))
                {
                    _PizzaOrders = base.CreateObjectSet<PizzaOrder>("PizzaOrders");
                }
                return _PizzaOrders;
            }
        }
        private ObjectSet<PizzaOrder> _PizzaOrders;
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<PizzaOrderDetail> PizzaOrderDetails
        {
            get
            {
                if ((_PizzaOrderDetails == null))
                {
                    _PizzaOrderDetails = base.CreateObjectSet<PizzaOrderDetail>("PizzaOrderDetails");
                }
                return _PizzaOrderDetails;
            }
        }
        private ObjectSet<PizzaOrderDetail> _PizzaOrderDetails;

        #endregion

        #region AddTo Methods
    
        /// <summary>
        /// Deprecated Method for adding a new object to the PizzaOrders EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToPizzaOrders(PizzaOrder pizzaOrder)
        {
            base.AddObject("PizzaOrders", pizzaOrder);
        }
    
        /// <summary>
        /// Deprecated Method for adding a new object to the PizzaOrderDetails EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToPizzaOrderDetails(PizzaOrderDetail pizzaOrderDetail)
        {
            base.AddObject("PizzaOrderDetails", pizzaOrderDetail);
        }

        #endregion

    }

    #endregion

    #region Entities
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="BillingModel", Name="PizzaOrder")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class PizzaOrder : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new PizzaOrder object.
        /// </summary>
        /// <param name="id">Initial value of the id property.</param>
        /// <param name="orderdate">Initial value of the orderdate property.</param>
        /// <param name="subtotal">Initial value of the Subtotal property.</param>
        /// <param name="gST">Initial value of the GST property.</param>
        /// <param name="total">Initial value of the Total property.</param>
        public static PizzaOrder CreatePizzaOrder(global::System.Int32 id, global::System.DateTime orderdate, global::System.Double subtotal, global::System.Double gST, global::System.Double total)
        {
            PizzaOrder pizzaOrder = new PizzaOrder();
            pizzaOrder.id = id;
            pizzaOrder.orderdate = orderdate;
            pizzaOrder.Subtotal = subtotal;
            pizzaOrder.GST = gST;
            pizzaOrder.Total = total;
            return pizzaOrder;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 id
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    OnidChanging(value);
                    ReportPropertyChanging("id");
                    _id = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("id");
                    OnidChanged();
                }
            }
        }
        private global::System.Int32 _id;
        partial void OnidChanging(global::System.Int32 value);
        partial void OnidChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.DateTime orderdate
        {
            get
            {
                return _orderdate;
            }
            set
            {
                OnorderdateChanging(value);
                ReportPropertyChanging("orderdate");
                _orderdate = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("orderdate");
                OnorderdateChanged();
            }
        }
        private global::System.DateTime _orderdate;
        partial void OnorderdateChanging(global::System.DateTime value);
        partial void OnorderdateChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Double Subtotal
        {
            get
            {
                return _Subtotal;
            }
            set
            {
                OnSubtotalChanging(value);
                ReportPropertyChanging("Subtotal");
                _Subtotal = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("Subtotal");
                OnSubtotalChanged();
            }
        }
        private global::System.Double _Subtotal;
        partial void OnSubtotalChanging(global::System.Double value);
        partial void OnSubtotalChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Double GST
        {
            get
            {
                return _GST;
            }
            set
            {
                OnGSTChanging(value);
                ReportPropertyChanging("GST");
                _GST = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("GST");
                OnGSTChanged();
            }
        }
        private global::System.Double _GST;
        partial void OnGSTChanging(global::System.Double value);
        partial void OnGSTChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Double Total
        {
            get
            {
                return _Total;
            }
            set
            {
                OnTotalChanging(value);
                ReportPropertyChanging("Total");
                _Total = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("Total");
                OnTotalChanged();
            }
        }
        private global::System.Double _Total;
        partial void OnTotalChanging(global::System.Double value);
        partial void OnTotalChanged();

        #endregion

    
        #region Navigation Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [XmlIgnoreAttribute()]
        [SoapIgnoreAttribute()]
        [DataMemberAttribute()]
        [EdmRelationshipNavigationPropertyAttribute("BillingModel", "FK_PizzaOrderDetails_PizzaOrders", "PizzaOrderDetail")]
        public EntityCollection<PizzaOrderDetail> PizzaOrderDetails
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedCollection<PizzaOrderDetail>("BillingModel.FK_PizzaOrderDetails_PizzaOrders", "PizzaOrderDetail");
            }
            set
            {
                if ((value != null))
                {
                    ((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedCollection<PizzaOrderDetail>("BillingModel.FK_PizzaOrderDetails_PizzaOrders", "PizzaOrderDetail", value);
                }
            }
        }

        #endregion

    }
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="BillingModel", Name="PizzaOrderDetail")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class PizzaOrderDetail : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new PizzaOrderDetail object.
        /// </summary>
        /// <param name="id">Initial value of the id property.</param>
        /// <param name="orderid">Initial value of the Orderid property.</param>
        /// <param name="pizzadescription">Initial value of the Pizzadescription property.</param>
        /// <param name="pieces">Initial value of the Pieces property.</param>
        /// <param name="totalPizzacost">Initial value of the TotalPizzacost property.</param>
        public static PizzaOrderDetail CreatePizzaOrderDetail(global::System.Int32 id, global::System.Int32 orderid, global::System.String pizzadescription, global::System.Int32 pieces, global::System.Double totalPizzacost)
        {
            PizzaOrderDetail pizzaOrderDetail = new PizzaOrderDetail();
            pizzaOrderDetail.id = id;
            pizzaOrderDetail.Orderid = orderid;
            pizzaOrderDetail.Pizzadescription = pizzadescription;
            pizzaOrderDetail.Pieces = pieces;
            pizzaOrderDetail.TotalPizzacost = totalPizzacost;
            return pizzaOrderDetail;
        }

        #endregion

        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 id
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    OnidChanging(value);
                    ReportPropertyChanging("id");
                    _id = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("id");
                    OnidChanged();
                }
            }
        }
        private global::System.Int32 _id;
        partial void OnidChanging(global::System.Int32 value);
        partial void OnidChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 Orderid
        {
            get
            {
                return _Orderid;
            }
            set
            {
                OnOrderidChanging(value);
                ReportPropertyChanging("Orderid");
                _Orderid = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("Orderid");
                OnOrderidChanged();
            }
        }
        private global::System.Int32 _Orderid;
        partial void OnOrderidChanging(global::System.Int32 value);
        partial void OnOrderidChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.String Pizzadescription
        {
            get
            {
                return _Pizzadescription;
            }
            set
            {
                OnPizzadescriptionChanging(value);
                ReportPropertyChanging("Pizzadescription");
                _Pizzadescription = StructuralObject.SetValidValue(value, false);
                ReportPropertyChanged("Pizzadescription");
                OnPizzadescriptionChanged();
            }
        }
        private global::System.String _Pizzadescription;
        partial void OnPizzadescriptionChanging(global::System.String value);
        partial void OnPizzadescriptionChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 Pieces
        {
            get
            {
                return _Pieces;
            }
            set
            {
                OnPiecesChanging(value);
                ReportPropertyChanging("Pieces");
                _Pieces = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("Pieces");
                OnPiecesChanged();
            }
        }
        private global::System.Int32 _Pieces;
        partial void OnPiecesChanging(global::System.Int32 value);
        partial void OnPiecesChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Double TotalPizzacost
        {
            get
            {
                return _TotalPizzacost;
            }
            set
            {
                OnTotalPizzacostChanging(value);
                ReportPropertyChanging("TotalPizzacost");
                _TotalPizzacost = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("TotalPizzacost");
                OnTotalPizzacostChanged();
            }
        }
        private global::System.Double _TotalPizzacost;
        partial void OnTotalPizzacostChanging(global::System.Double value);
        partial void OnTotalPizzacostChanged();

        #endregion

    
        #region Navigation Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [XmlIgnoreAttribute()]
        [SoapIgnoreAttribute()]
        [DataMemberAttribute()]
        [EdmRelationshipNavigationPropertyAttribute("BillingModel", "FK_PizzaOrderDetails_PizzaOrders", "PizzaOrder")]
        public PizzaOrder PizzaOrder
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<PizzaOrder>("BillingModel.FK_PizzaOrderDetails_PizzaOrders", "PizzaOrder").Value;
            }
            set
            {
                ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<PizzaOrder>("BillingModel.FK_PizzaOrderDetails_PizzaOrders", "PizzaOrder").Value = value;
            }
        }
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [BrowsableAttribute(false)]
        [DataMemberAttribute()]
        public EntityReference<PizzaOrder> PizzaOrderReference
        {
            get
            {
                return ((IEntityWithRelationships)this).RelationshipManager.GetRelatedReference<PizzaOrder>("BillingModel.FK_PizzaOrderDetails_PizzaOrders", "PizzaOrder");
            }
            set
            {
                if ((value != null))
                {
                    ((IEntityWithRelationships)this).RelationshipManager.InitializeRelatedReference<PizzaOrder>("BillingModel.FK_PizzaOrderDetails_PizzaOrders", "PizzaOrder", value);
                }
            }
        }

        #endregion

    }

    #endregion

    
}
