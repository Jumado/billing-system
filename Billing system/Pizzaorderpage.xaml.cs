﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Billing_system
{
    /// <summary>
    /// Interaction logic for Pizzaorderpage.xaml
    /// </summary>
    public partial class Pizzaorderpage : Page
    {
        private ArrayList Toppings;
        public Pizzaorderpage()
        {
            InitializeComponent();
            //small cheese handlers
            this.rdoLarge.Checked += new RoutedEventHandler(rdoLarge_Checked);
            this.rdoMedium.Checked += new RoutedEventHandler(rdoMedium_Checked);
            this.Toppings = new ArrayList();
            this.chksmallcheese.Checked += new RoutedEventHandler(chksmallcheese_Checked);
            this.chksmallPepperoni.Checked += new RoutedEventHandler(chksmallPepperoni_Checked);
            this.chksmallHam.Checked += new RoutedEventHandler(chksmallHam_Checked);
            this.chksmallPineapple.Checked += new RoutedEventHandler(chksmallPineapple_Checked);
            this.chksmallSausage.Checked += new RoutedEventHandler(chksmallSausage_Checked);
            this.chksmallFeta.Checked += new RoutedEventHandler(chksmallFeta_Checked);
            this.chksmallTomatoes.Checked += new RoutedEventHandler(chksmallTomatoes_Checked);
            this.chksmallOlives.Checked += new RoutedEventHandler(chksmallOlives_Checked);
            this.chksmallcheese.Unchecked += new RoutedEventHandler(chksmallcheese_Unchecked);
            this.chksmallPepperoni.Unchecked += new RoutedEventHandler(chksmallPepperoni_Unchecked);
            this.chksmallHam.Unchecked += new RoutedEventHandler(chksmallHam_Unchecked);
            this.chksmallPineapple.Unchecked += new RoutedEventHandler(chksmallPineapple_Unchecked);
            this.chksmallSausage.Unchecked += new RoutedEventHandler(chksmallSausage_Unchecked);
            this.chksmallFeta.Unchecked += new RoutedEventHandler(chksmallFeta_Unchecked);
            this.chksmallTomatoes.Unchecked += new RoutedEventHandler(chksmallTomatoes_Unchecked);
            this.chksmallOlives.Unchecked += new RoutedEventHandler(chksmallOlives_Unchecked);
            //medium cheese handlers
            this.chkmediumCheese.Checked += new RoutedEventHandler(chkmediumCheese_Checked);
            this.chkmediumCheese.Unchecked += new RoutedEventHandler(chkmediumCheese_Unchecked);
            this.chkmediumPepperoni.Checked += new RoutedEventHandler(chkmediumPepperoni_Checked);
            this.chkmediumPepperoni.Unchecked += new RoutedEventHandler(chkmediumPepperoni_Unchecked);
            this.chkmediumHam.Checked += new RoutedEventHandler(chkmediumHam_Checked);
            this.chkmediumHam.Unchecked += new RoutedEventHandler(chkmediumHam_Unchecked);
            this.chkmediumPineapple.Checked += new RoutedEventHandler(chkmediumPineapple_Checked);
            this.chkmediumPineapple.Unchecked += new RoutedEventHandler(chkmediumPineapple_Unchecked);
            this.chkmediumSausage.Checked += new RoutedEventHandler(chkmediumSausage_Checked);
            this.chkmediumSausage.Unchecked += new RoutedEventHandler(chkmediumSausage_Unchecked);
            this.chkmediumFeta.Checked += new RoutedEventHandler(chkmediumFeta_Checked);
            this.chkmediumFeta.Unchecked += new RoutedEventHandler(chkmediumFeta_Unchecked);
            this.chkmediumTomatoes.Checked += new RoutedEventHandler(chkmediumTomatoes_Checked);
            this.chkmediumTomatoes.Unchecked += new RoutedEventHandler(chkmediumTomatoes_Unchecked);
            this.chkmediumOlives.Checked += new RoutedEventHandler(chkmediumOlives_Checked);
            this.chkmediumOlives.Unchecked += new RoutedEventHandler(chkmediumOlives_Unchecked);
            //Large cheese handlers
            this.chklargeCheese.Checked += new RoutedEventHandler(chklargeCheese_Checked);
            this.chklargeCheese.Unchecked += new RoutedEventHandler(chklargeCheese_Unchecked);
            this.chklargePepperoni.Checked += new RoutedEventHandler(chklargePepperoni_Checked);
            this.chklargePepperoni.Unchecked += new RoutedEventHandler(chklargePepperoni_Unchecked);
            this.chklargeHam.Checked += new RoutedEventHandler(chklargeHam_Checked);
            this.chklargeHam.Unchecked += new RoutedEventHandler(chklargeHam_Unchecked);
            this.chklargePineapple.Checked += new RoutedEventHandler(chklargePineapple_Checked);
            this.chklargePineapple.Unchecked += new RoutedEventHandler(chklargePineapple_Unchecked);
            this.chklargeSausage.Checked += new RoutedEventHandler(chklargeSausage_Checked);
            this.chklargeSausage.Unchecked += new RoutedEventHandler(chklargeSausage_Unchecked);
            this.chklargeFeta.Checked += new RoutedEventHandler(chklargeFeta_Checked);
            this.chklargeFeta.Unchecked += new RoutedEventHandler(chklargeFeta_Unchecked);
            this.chklargeTomatoes.Checked += new RoutedEventHandler(chklargeTomatoes_Checked);
            this.chklargeTomatoes.Unchecked += new RoutedEventHandler(chklargeTomatoes_Unchecked);
            this.chklargeOlives.Checked += new RoutedEventHandler(chklargeOlives_Checked);
            this.chklargeOlives.Unchecked += new RoutedEventHandler(chklargeOlives_Unchecked);
            this.btnReset.Click += new RoutedEventHandler(btnReset_Click);
        }

        void btnReset_Click(object sender, RoutedEventArgs e)
        {
            this.ClearLargeToppings(true);
            this.ClearMediumToppings(true);
            this.ClearSmallToppings(true);
            this.txtcost.Clear();
            this.txtpizzadesc.Clear();
            this.txtpizzaqty.Clear();
            this.Toppings.Clear();
            this.rdoLarge.IsChecked = false;
            this.rdoMedium.IsChecked = false;
            this.rdoSmall.IsChecked = false;
        }

        void chklargeOlives_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Olives");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargeOlives.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargeTomatoes_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Tomatoes");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargeTomatoes.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargeOlives_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargeOlives.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Olives");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargeOlives.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chklargeTomatoes_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargeTomatoes.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Tomatoes");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargeTomatoes.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }

        }

        void chklargeFeta_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Feta Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargeFeta.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargeFeta_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargeFeta.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Feta Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargeFeta.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chklargeSausage_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Sausage");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargeSausage.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargeSausage_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargeSausage.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Sausage");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargeSausage.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chklargePineapple_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargePineapple.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Pineapple");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargePineapple.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chklargePineapple_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Pineapple");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargePineapple.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargeHam_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Ham");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargeHam.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargeHam_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargeHam.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Ham");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargeHam.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chklargePepperoni_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Pepperoni");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargePepperoni.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargePepperoni_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargePepperoni.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Pepperoni");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargePepperoni.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chklargeCheese_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chklargeCheese.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chklargeCheese_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoLarge.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chklargeCheese.IsChecked = false;
                this.rdoLarge.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Large");
                this.txtcost.Text = (double.Parse(chklargeCheese.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumOlives_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Olives");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumOlives.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumOlives_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumOlives.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Olives");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumOlives.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumTomatoes_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Tomatoes");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumTomatoes.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumTomatoes_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumTomatoes.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Tomatoes");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumTomatoes.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumFeta_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Feta Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumFeta.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumFeta_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumFeta.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Feta Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumFeta.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumSausage_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Sausage");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumSausage.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumSausage_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumSausage.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Sausage");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumSausage.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumPineapple_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Pineapple");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumPineapple.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumPineapple_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumPineapple.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Pineapple");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumPineapple.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumHam_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Ham");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumHam.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumPepperoni_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Pepperoni");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumPepperoni.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumHam_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumHam.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Ham");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumHam.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumPepperoni_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumPepperoni.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Pepperoni");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumPepperoni.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chkmediumCheese_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chkmediumCheese.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chkmediumCheese_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoMedium.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chkmediumCheese.IsChecked = false;
                this.rdoMedium.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Medium");
                this.txtcost.Text = (double.Parse(chkmediumCheese.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chksmallOlives_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Olives");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chksmallOlives.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chksmallTomatoes_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Tomatoes");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chksmallTomatoes.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chksmallFeta_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Feta Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chksmallFeta.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chksmallSausage_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Sausage");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chksmallSausage.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chksmallPineapple_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Pineapple");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chksmallPineapple.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chksmallHam_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Ham");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chksmallHam.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chksmallPepperoni_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Pepperoni");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim()) - double.Parse(chksmallPepperoni.Tag.ToString())).ToString("0.00");

            }
            catch
            {

            }
        }

        void chksmallcheese_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Toppings.Remove("Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(this.txtcost.Text.Trim())-double.Parse(chksmallcheese.Tag.ToString()) ).ToString("0.00"); 
              
            }
            catch { 
            
            }
        }

        void chksmallOlives_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoSmall.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallHam.IsChecked = false;
                this.rdoSmall.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Olives");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallOlives.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00");

            }
        }

        void chksmallTomatoes_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoSmall.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallTomatoes.IsChecked = false;
                this.rdoSmall.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Tomatoes");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallTomatoes.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00"); 
            }
        }

        void chksmallFeta_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoSmall.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallFeta.IsChecked = false;
                this.rdoSmall.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Feta Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallFeta.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00"); 
            }
        }

        void chksmallSausage_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoSmall.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallSausage.IsChecked = false;
                this.rdoSmall.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Sausage");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallSausage.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00"); 
              
            }
        }

        void chksmallPineapple_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoSmall.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallPineapple.IsChecked = false;
                this.rdoSmall.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Pineapple");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallPineapple.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00"); 
              
            }
        }

        void chksmallHam_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoSmall.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallHam.IsChecked = false;
                this.rdoSmall.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else
            {

                this.Toppings.Add("Ham");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallHam.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00"); 
              
            }
        }

        void chksmallPepperoni_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.chksmallPepperoni.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallPepperoni.IsChecked = false;
                this.rdoSmall.Focus();
            }
            else
            {

                this.Toppings.Add("Pepperoni");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallPepperoni.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00"); 
              
            }
        }

        void chksmallcheese_Checked(object sender, RoutedEventArgs e)
        {
            if (!(bool)this.rdoSmall.IsChecked)
            {
                MessageBox.Show("Please select a pizza size first", "Size", MessageBoxButton.OK, MessageBoxImage.Warning);
                chksmallcheese.IsChecked = false;
                this.rdoSmall.Focus();
                this.txtpizzadesc.Clear(); 
            }
            else {
              
                this.Toppings.Add("Cheese");
                this.txtpizzadesc.Text = ToppingsInfo("Small");
                this.txtcost.Text = (double.Parse(chksmallcheese.Tag.ToString()) + double.Parse(this.txtcost.Text.Trim())).ToString("0.00"); 
              
            }
        }

        void rdoMedium_Checked(object sender, RoutedEventArgs e)
        {
            this.txtpizzaqty.Clear();
            this.txtcost.Clear();
            this.txtpizzadesc.Clear();
            this.txtpizzadesc.Text = "Medium pizza";
            this.txtcost.Text = double.Parse(this.rdoMedium.Tag.ToString()).ToString("#0.00");  
            this.ClearMediumToppings(true);
            this.ClearSmallToppings(false);
            this.ClearLargeToppings(false);
            this.Toppings.Clear();
        }

        void rdoLarge_Checked(object sender, RoutedEventArgs e)
        {
            this.txtpizzaqty.Clear();
            this.txtcost.Clear();
            this.txtpizzadesc.Clear();
            this.txtpizzadesc.Text = "Large pizza";
            this.txtcost.Text = double.Parse(this.rdoLarge.Tag.ToString()).ToString("#0.00");   
            this.ClearMediumToppings(false);
            this.ClearSmallToppings(false);
            this.ClearLargeToppings(true);
            this.Toppings.Clear();
        }

        private void rdoSmall_Checked(object sender, RoutedEventArgs e)
        {
            this.txtpizzaqty.Clear();
            this.txtcost.Clear();
            this.txtpizzadesc.Clear();
            this.txtpizzadesc.Text = "Small pizza";
            this.txtcost.Text = double.Parse(this.rdoSmall.Tag.ToString()).ToString("#0.00");  
            this.ClearMediumToppings(false);
            this.ClearSmallToppings(true);
            this.ClearLargeToppings(false);
            this.Toppings.Clear();
        }
        private void ClearLargeToppings(bool rdoEnable) {
            this.chklargeCheese.IsChecked= false;
            this.chklargeCheese.IsEnabled = rdoEnable;
            this.chklargePepperoni.IsChecked = false;
            this.chklargePepperoni.IsEnabled = rdoEnable;
            this.chklargeHam.IsChecked = false;
            this.chklargeHam.IsEnabled = rdoEnable;
            this.chklargePineapple.IsEnabled = rdoEnable;
            this.chklargePineapple.IsChecked = false;
            this.chklargeSausage.IsChecked = false;
            this.chklargeSausage.IsEnabled = rdoEnable;
            this.chklargeFeta.IsEnabled = rdoEnable;
            this.chklargeFeta.IsChecked = false;
            this.chklargeTomatoes.IsChecked = false;
            this.chklargeTomatoes.IsEnabled = rdoEnable;
            this.chklargeOlives.IsChecked = false;
            this.chklargeOlives.IsEnabled = rdoEnable;
            
        }
        private void ClearMediumToppings(bool rdoEnable)
        {
            this.chkmediumCheese.IsChecked = false;
            this.chkmediumCheese.IsEnabled = rdoEnable;
            this.chkmediumPepperoni.IsChecked = false;
            this.chkmediumPepperoni.IsEnabled = rdoEnable;
            this.chkmediumHam.IsChecked = false;
            this.chkmediumHam.IsEnabled = rdoEnable;
            this.chkmediumPineapple.IsEnabled = rdoEnable;
            this.chkmediumPineapple.IsChecked = false;
            this.chkmediumSausage.IsChecked = false;
            this.chkmediumSausage.IsEnabled = rdoEnable;
            this.chkmediumFeta.IsEnabled = rdoEnable;
            this.chkmediumFeta.IsChecked = false;
            this.chkmediumTomatoes.IsChecked = false;
            this.chkmediumTomatoes.IsEnabled = rdoEnable;
            this.chkmediumOlives.IsChecked = false;
            this.chkmediumOlives.IsEnabled = rdoEnable;
           
        }
        private void ClearSmallToppings(bool rdoEnable)
        {
            this.chksmallcheese.IsChecked = false;
            this.chksmallcheese.IsEnabled = rdoEnable;
            this.chksmallPepperoni.IsChecked = false;
            this.chksmallPepperoni.IsEnabled = rdoEnable;
            this.chksmallHam.IsChecked = false;
            this.chksmallHam.IsEnabled = rdoEnable;
            this.chksmallPineapple.IsEnabled = rdoEnable;
            this.chksmallPineapple.IsChecked = false;
            this.chksmallSausage.IsChecked = false;
            this.chksmallSausage.IsEnabled = rdoEnable;
            this.chksmallFeta.IsEnabled = rdoEnable;
            this.chksmallFeta.IsChecked = false;
            this.chksmallTomatoes.IsChecked = false;
            this.chksmallTomatoes.IsEnabled = rdoEnable;
            this.chksmallOlives.IsChecked = false;
            this.chksmallOlives.IsEnabled = rdoEnable;
           
        }
        private string ToppingsInfo(string pizzasize) {
            int i = 1;
            StringBuilder topping = new StringBuilder();
            if (this.Toppings.Count == 0) return string.Concat(pizzasize, " pizza");  
            foreach (string topin in this.Toppings) {
                if (i == 1) topping.Append(string.Concat(",", NumericStrings.GetNumberWords(this.Toppings.Count), " topping pizza", "-", topin));
                else if ((i < this.Toppings.Count)) { 
                    topping.Append(string.Concat(",",topin));
                }
                else if (i == this.Toppings.Count) {
                    topping.Append(string.Concat(" and ", topin));
                }
                i++;
            }
           
            return string.Concat(pizzasize,topping.ToString());  
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtpizzadesc.Text.Trim())){
                MessageBox.Show("Start by selecting a pizza size","Pizza",MessageBoxButton.OK ,MessageBoxImage.Warning );  
            }else if((string.IsNullOrEmpty(this.txtpizzaqty.Text.Trim())) || (!Regex.IsMatch(this.txtpizzaqty.Text.Trim(), @"^\d+$"))){
                MessageBox.Show("Please specify a numeric order quantity","Order quantity",MessageBoxButton.OK,MessageBoxImage.Warning );
                this.txtpizzaqty.Focus();
            }else{
            
            try
            {
                PizzaOrderDetail orderitm = new PizzaOrderDetail();
                orderitm.Pizzadescription = this.txtpizzadesc.Text.Trim();
                orderitm.Pieces = int.Parse(this.txtpizzaqty.Text.Trim());
                orderitm.TotalPizzacost = double.Parse(this.txtcost.Text.Trim()) * double.Parse(this.txtpizzaqty.Text.Trim());
                if (this.dgOrdercart.ItemsSource == null)
                {
                    List<PizzaOrderDetail> Orderitems = new List<PizzaOrderDetail>();
                    Orderitems.Add(orderitm); 
                    this.dgOrdercart.BeginEdit();
                    this.dgOrdercart.ItemsSource = Orderitems;
                    this.dgOrdercart.EndInit();
                    this.dgOrdercart.Items.Refresh();
                } else {
                    List<PizzaOrderDetail> Orderitems = (List < PizzaOrderDetail >) this.dgOrdercart.ItemsSource;
                    PizzaOrderDetail searchitm = Orderitems.Where(itm => itm.Pizzadescription.Equals(orderitm.Pizzadescription)).FirstOrDefault();
                    if (searchitm == null)
                    {
                        Orderitems.Add(orderitm); 
                        this.dgOrdercart.BeginEdit();
                        this.dgOrdercart.ItemsSource = Orderitems;
                        this.dgOrdercart.EndInit();
                        this.dgOrdercart.Items.Refresh();
                    }
                    else {
                        this.dgOrdercart.BeginEdit();
                        searchitm.Pieces += orderitm.Pieces;
                        searchitm.TotalPizzacost += orderitm.TotalPizzacost;
                        this.dgOrdercart.EndInit();
                        this.dgOrdercart.Items.Refresh();
                    }
                }
                this.ClearLargeToppings(true);
                this.ClearMediumToppings(true);
                this.ClearSmallToppings(true);
                this.rdoLarge.IsChecked = false;
                this.rdoMedium.IsChecked = false;
                this.rdoSmall.IsChecked = false;
                this.txtcost.Clear();
                this.txtpizzadesc.Clear();
                this.txtpizzaqty.Clear(); 
            }
            catch {
                MessageBox.Show("An error occured while adding item to cart", "Order cart", MessageBoxButton.OK, MessageBoxImage.Warning);   
            }
            }    
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.dgOrdercart.ItemsSource = null; 
            }
            catch {
                MessageBox.Show("An error occured while clearing order cart", "Order cart", MessageBoxButton.OK, MessageBoxImage.Warning);   
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (this.dgOrdercart.ItemsSource == null)
            {
                MessageBox.Show("Your order cart is empty,please add items first", "Order cart", MessageBoxButton.OK, MessageBoxImage.Warning);
            } else {
                try
                {
                    this.btnSave.IsEnabled = false;
                    PizzaOrder NewOrder = new PizzaOrder();
                    NewOrder.orderdate = DateTime.Now;
                    List<PizzaOrderDetail> Orderitems = (List<PizzaOrderDetail>)this.dgOrdercart.ItemsSource;
                    double Subtotal = Orderitems.Sum(itm => itm.TotalPizzacost);
                    double GST = Math.Round(0.05 * Subtotal, 2);
                    double Total = GST + Subtotal;
                    NewOrder.GST = GST;
                    NewOrder.Subtotal = Subtotal;
                    NewOrder.Total = Total;
                    Orderitems.ForEach(orditm => NewOrder.PizzaOrderDetails.Add(orditm));
                    this.stkpnlprogress.Visibility = Visibility.Visible;
                    int? Result = NewOrder.SaveOrder();
                    if (Result.HasValue)
                    {
                        PrintDialog printdlg = new PrintDialog();
                        if (printdlg.ShowDialog().Value)
                        {
                            PizzaOrder ord = new PizzaOrder(Result.Value).Orderinformation();
                            IDocumentPaginatorSource idpSource = PrintHelper.CreateFlowDocument(ord);
                            printdlg.PrintDocument(idpSource.DocumentPaginator, "Receipt");
                            this.stkpnlprogress.Visibility = Visibility.Hidden;
                            MessageBox.Show("Transaction posted and receipt sent to printer successfully", "Receipt", MessageBoxButton.OK, MessageBoxImage.Information);

                        }else {
                            MessageBox.Show("Transaction posted to the database successfully", "Order", MessageBoxButton.OK, MessageBoxImage.Information);

                        }
                        this.dgOrdercart.ItemsSource = null;
                        this.ClearLargeToppings(true);
                        this.ClearMediumToppings(true);
                        this.ClearSmallToppings(true);
                        this.rdoLarge.IsChecked = false;
                        this.rdoMedium.IsChecked = false;
                        this.rdoSmall.IsChecked = false;
                        this.txtcost.Clear();
                        this.txtpizzadesc.Clear();
                        this.txtpizzaqty.Clear();
                      
                    } else {
                        MessageBox.Show("Transaction information could not be posted", "Database error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                catch
                {
                    this.stkpnlprogress.Visibility = Visibility.Hidden;
                    MessageBox.Show("An error occured while printing order receipt", "Receipt", MessageBoxButton.OK, MessageBoxImage.Warning);
                }finally {
                    this.btnSave.IsEnabled = true;
                    this.stkpnlprogress.Visibility = Visibility.Hidden;
                }
               
            } 
        }

        private void txtpizzaqty_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) {
                if ((Regex.IsMatch(this.txtpizzaqty.Text.Trim(), @"^\d+$")) && (!string.IsNullOrEmpty(this.txtpizzadesc.Text.Trim())))
                {
                    btnAdd_Click(sender, e);
                }
            }
            else if (e.Key == Key.Escape) {
                btnReset_Click(sender, e);
            }
        }

       
    }
}
