﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient; 
using System.Data.EntityClient;
namespace Billing_system
{
   public partial class PizzaOrder
    {
       
      private string EntityConnectionString (){
         
              EntityConnectionStringBuilder entitystrbuilder = new EntityConnectionStringBuilder();
              SqlConnectionStringBuilder constr = new SqlConnectionStringBuilder();
              string providername = "System.Data.SqlClient";
              constr.DataSource = Properties.Settings.Default.server.Trim();
              constr.InitialCatalog = Properties.Settings.Default.database.Trim();
              constr.UserID = Properties.Settings.Default.userid.Trim();
              constr.Password = Properties.Settings.Default.password.Trim();
              entitystrbuilder.Provider = providername;
              entitystrbuilder.ProviderConnectionString = constr.ToString();
              entitystrbuilder.Metadata = @"res://*/BillingEntities.csdl|res://*/BillingEntities.ssdl|res://*/BillingEntities.msl";
              return entitystrbuilder.ToString();  
          
       }
      public int? SaveOrder() {
          try
          {
              using (Billing_system.Entities ctx = new Billing_system.Entities(this.EntityConnectionString())) {
                  ctx.PizzaOrders.AddObject(this);
                  ctx.SaveChanges();
                  return this.id;
              };
            
          }
          catch {
              return null;
          }
       
      }
      public  PizzaOrder Orderinformation() {
          try
          {
              using (Billing_system.Entities ctx = new Billing_system.Entities(this.EntityConnectionString()))
              {
                  return ctx.PizzaOrders.Include("PizzaOrderDetails").Where(ord => ord.id == this.id).FirstOrDefault();
              };
          }catch {
              return null;
          }
      }
      public PizzaOrder() { }
      public PizzaOrder(int ordid) {
          this.id = ordid;
      }
    }
}
