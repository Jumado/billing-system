﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Billing_system
{
 sealed class NumericStrings{
    public static string DecimalSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
    public static string GroupSeparator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
    public static string SpaceString  = " ";
    public static string AndString = "and";
    public static string DashString  = "-";
    public static string DecimalString  = "point";
    public static string NegativeString = "negative";
    public enum RootNumbers { 
     zero, one,two,three,four,five,six,seven,eight,nine ,ten,eleven,twelve
     ,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen ,nineteen,twenty,thirty = 30
      ,forty = 40, fifty = 50,sixty = 60,seventy = 70 ,eighty = 80, ninety = 90, hundred = 100
          ,thousand = 1000,million = 1000000,billion = 1000000000
    }
    public static string GetRootNumberWord(int number){
        return Enum.GetName(typeof(RootNumbers), number);
     }
    public  static string GetNumberWords( int number ){
        if (number == 0) return GetRootNumberWord(number);
        if (number < 0)  return NegativeString + SpaceString + GetNumberWords(System.Math.Abs(number));
        StringBuilder result = new StringBuilder();
        int digitIndex = 9;
        while (digitIndex > 1) {
            int digitValue = (int)(Math.Pow(10,digitIndex) );
                 if ((number/digitValue) > 0){
                     result.Append(GetNumberWords(number/digitValue));
                     result.Append(SpaceString);
                     result.Append(GetRootNumberWord(digitValue));
                     result.Append(SpaceString);
                     number = (number % digitValue);
                  }
                if (digitIndex == 9) {
                     digitIndex = 6;
                  }else if (digitIndex == 6){
                      digitIndex = 3;
                  }
                else if (digitIndex == 3)
                {
                    digitIndex = 2;
                }
                else {

                    digitIndex = 0;
                }
               
          
              
           
             }
        
        if (number > 0) {
            if (result.Length > 0){
                result.Append(AndString);
                result.Append(SpaceString);
            }

            if (number < 20) {
                result.Append(GetRootNumberWord(number));
            }else{
                result.Append(GetRootNumberWord((number/ 10) * 10));
                int modTen  = number % 10;
                if (modTen > 0) {
                    result.Append(DashString);
                    result.Append(GetRootNumberWord(modTen));
                }
            }
        }
        return result.ToString();
         }
  
     
     }

}
